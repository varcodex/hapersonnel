export interface Rider {
    cart_id: number;
    fleet_job_id: string;
    fleet_rider_id: number;
    fleet_rider_name: string;
    fleet_status: string;
}

import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { EMPTY, Observable, throwError } from 'rxjs';
import { LoadingController } from '@ionic/angular';
import { catchError, delay, finalize, map, retryWhen, take, tap } from 'rxjs/operators';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor{

    constructor(private loadingctrl: LoadingController){}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        this.loadingctrl.getTop().then(hasLoading => {
            if (!hasLoading){
                this.loadingctrl.create({
                    spinner: 'circular',
                    translucent: true
                }).then(loading => loading.present());
            }
        });

        return next.handle(request).pipe(
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    switch ((<HttpErrorResponse>err).status) {
                        case 403:
                            alert((<HttpErrorResponse>err).error.message);
                        // tslint:disable-next-line: no-switch-case-fall-through
                        default:
                            return throwError(err);
                    }
                } else {
                    return throwError(err);
                }
            }),
            retryWhen(err => {
                let retries = 1;
                return err.pipe(
                    delay(1000),
                    tap(() => {
                        console.log('retry');
                    }),
                    map(error => {
                        if (retries++ === 1){
                            throw error;
                        }
                        return error;
                    })
                );
            }),
            catchError(err => {
                console.log('error: ', err);
                return EMPTY;
            }),
            finalize(() => {
                
                this.loadingctrl.getTop().then(hasLoading => {
                    if (hasLoading){
                        this.loadingctrl.dismiss();
                    }
                });
            })
        );
    }
}

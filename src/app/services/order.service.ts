import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { Order } from '../models/order';
import { Rider } from '../models/rider';
import { Product } from '../models/product';
import { DataServiceService } from './data-service.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  public headers: Headers;
  private token;

  constructor(private http: HttpClient,
              private dataServiceService: DataServiceService) {
        this.dataServiceService.castToken.subscribe(data => this.token = data);
  }

  getAvailableOnForDeliveryOrders(token): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Order[]>(`${environment.api.ha}/get/orders/delivery`, '', httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  getAvailableOnForDeliveryOrdersLBC(token): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Order[]>(`${environment.api.ha}/get/orders/delivery/lbc`, '', httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  getAvailableOnForDeliveryOrdersLastMile(token): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Order[]>(`${environment.api.ha}/get/orders/delivery/lastmile`, '', httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  getJobsPerCart(token, params): Observable<Rider[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Rider[]>(`${environment.api.ha}/lastmile/jobsPerCart`, JSON.stringify(params), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  getJobStatus(token, params): Observable<Rider[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Rider[]>(`${environment.api.ha}/lastmile/getJobStatus`, JSON.stringify(params), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  getRiders(token): Observable<Rider[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.get<Rider[]>(`${environment.api.ha}/lastmile/ridersList`, httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  getBoxCategory(token): Observable<Rider[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.get<Rider[]>(`${environment.api.ha}/get/box-categories`, httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  updateBoxCategory(token, params): Observable<Rider[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Rider[]>(`${environment.api.ha}/update/order/box-categories`, JSON.stringify(params), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  updateJobs(token, params): Observable<Rider[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Rider[]>(`${environment.api.ha}/lastmile/updateJob`, JSON.stringify(params), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  getOrderProducts(token, params): Observable<Product[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Product[]>(`${environment.api.pickers}/get/order/products`, JSON.stringify(params), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  updateOrder(token, params): Observable<Order[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Order[]>(`${environment.api.ha}/update/order`, JSON.stringify(params), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.message}`;
    }
    alert(errorMessage);
    return throwError(errorMessage);
  }
}

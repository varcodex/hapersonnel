import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  private token = new BehaviorSubject<string>(null);
  private role = new BehaviorSubject<string>(null);
  private order = new BehaviorSubject<string[]>(null);
  private forDeliveryOrders = new BehaviorSubject<string[]>(null);
  private haName = new BehaviorSubject<string>(null);
  private haId = new BehaviorSubject<string>(null);
  private jobOrderNo = new BehaviorSubject<string>(null);
  private areaOfDelivery = new BehaviorSubject<string>(null);
  private deliveryDate = new BehaviorSubject<string>(null);
  private OrderStatusId = new BehaviorSubject<string>(null);
  private CustomerName = new BehaviorSubject<string>(null);
  castCustomerName = this.CustomerName.asObservable();
  castOrder = this.order.asObservable();
  castToken = this.token.asObservable();
  castRole = this.role.asObservable();
  casthaName = this.haName.asObservable();
  casthaId = this.haId.asObservable();
  castjobOrderNo = this.jobOrderNo.asObservable();
  castareaOfDelivery = this.areaOfDelivery.asObservable();
  castdeliveryDate = this.deliveryDate.asObservable();
  castOrderStatusId = this.OrderStatusId.asObservable();
  constructor(private storage: Storage) { }

  setForDeliveryOrders(forDeliveryOrders){
    this.storage.set('forDeliveryOrders', forDeliveryOrders);
    this.order.next(forDeliveryOrders);
  }
  setOrder(order){
    this.storage.set('order', order);
    this.order.next(order);
  }
  setjobOrderNo(jobOrderNo){
    this.storage.set('jobOrderNo', jobOrderNo);
    this.jobOrderNo.next(jobOrderNo);
  }
  setOrderStatusId(OrderStatusId){
    this.storage.set('OrderStatusId', OrderStatusId);
    this.OrderStatusId.next(OrderStatusId);
  }
  setareaOfDelivery(areaOfDelivery){
    this.storage.set('areaOfDelivery', areaOfDelivery);
    this.areaOfDelivery.next(areaOfDelivery);
  }
  setdeliveryDate(deliveryDate){
    this.storage.set('deliveryDate', deliveryDate);
    this.deliveryDate.next(deliveryDate);
  }
  setCustomerName(CustomerName){
    this.storage.set('CustomerName', CustomerName);
    this.CustomerName.next(CustomerName);
  }
  setToken(token){
    this.storage.set('token', token);
    this.token.next(token);
  }
  setRole(role){
    this.storage.set('role', role);
    this.role.next(role);
  }
  sethaName(haName){
    this.storage.set('haName', haName);
    this.haName.next(haName);
  }
  sethaId(haId){
    this.storage.set('haId', haId);
    this.haId.next(haId);
  }

  getOrderStatusId(){
    this.storage.get('OrderStatusId').then((data) => {
      this.OrderStatusId.next(data);
    });
  }
  getjobOrderNo(){
    this.storage.get('jobOrderNo').then((data) => {
      this.jobOrderNo.next(data);
    });
  }
  getOrder(){
    this.storage.get('order').then((data) => {
      this.order.next(data);
    });
  }
  getForDeliveryOrder(){
    this.storage.get('forDeliveryOrder').then((data) => {
      this.order.next(data);
    });
  }
  getareaOfDelivery(){
    this.storage.get('areaOfDelivery').then((data) => {
      this.areaOfDelivery.next(data);
    });
  }
  getdeliveryDate(){
    this.storage.get('deliveryDate').then((data) => {
      this.deliveryDate.next(data);
    });
  }
  getCustomerName(){
    this.storage.get('CustomerName').then((data) => {
      this.CustomerName.next(data);
    });
  }
  getToken(){
    this.storage.get('token').then((data) => {
      this.token.next(data);
    });
  }
  getRole(){
    this.storage.get('role').then((data) => {
      this.role.next(data);
    });
  }
  gethaName(){
    this.storage.get('haName').then((data) => {
      this.haName.next(data);
    });
  }
  gethaId(){
    this.storage.get('haId').then((data) => {
      this.haId.next(data);
    });
  }
}

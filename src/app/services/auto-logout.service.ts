import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Injectable, NgZone } from '@angular/core';
import * as store from 'store';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

const MINUTES_UNITL_AUTO_LOGOUT = 5 // in Minutes
const CHECK_INTERVALL = 1000 // in ms
const STORE_KEY = 'lastAction';

@Injectable()
export class AutoLogoutService {
  token: any;
  isLoggedIn: boolean;
  id: any;

  constructor(
    private auth: AuthService,
    private router: Router,
    private ngZone: NgZone,
    private storage: Storage,
    public alertController: AlertController
  ) {
    this.check();
    this.initListener();
    this.initInterval();
  }

  get lastAction() {
    return parseInt(store.get(STORE_KEY));
  }
  set lastAction(value) {
    store.set(STORE_KEY, value);
  }

  initListener() {
    this.ngZone.runOutsideAngular(() => {
      document.body.addEventListener('click', () => this.reset());
    });
  }

  initInterval() {
    this.ngZone.runOutsideAngular(() => {
      this.id = setInterval(() => {
        this.check();
      }, CHECK_INTERVALL);
    })
  }

  reset() {
    this.lastAction = Date.now();
  }

   async check() {
    const now = Date.now();
    const timeleft = this.lastAction + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;
    this.token = await this.getStorage('token');
    if(this.token != null){
      this.isLoggedIn = true;
       this.ngZone.run( async () => {
        if (isTimeout && this.isLoggedIn) {
          clearInterval(this.id);
          this.presentAlertConfirm();
        }else{
        }
      });
    }else{
      this.reset();
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert!',
      message: 'You have <strong>NO ACTIVITY</strong> for the last 5 minutes. Do you want to LOG OUT?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'danger',
          handler: (blah) => {
            this.initListener();
            this.initInterval();
          }
        }, {
          text: 'Yes',
          handler: async () => {
            this.auth.onLogout(this.token);
            this.router.navigate(['login']);
            await this.clearStorage();
            this.initListener();
            this.initInterval();
          }
        }
      ]
    });

    await alert.present();
  }

  async clearStorage(): Promise<any> {
    try {
        const result =  await this.storage.clear();
        return result;
    }
    catch(e) { console.log(e) }
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch (e) { console.log(e); }
  }
}
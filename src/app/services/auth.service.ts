import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Auth } from '../models/auth';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public token: any;
  public headers: Headers;

  constructor(private http: HttpClient) { }

  onLogin(credentials): Observable<Auth[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': '*',
        Accept: 'application/json, text/plain'
      })
    };

    return this.http.post<Auth[]>(`${environment.api.ha}/login`, JSON.stringify(credentials), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  onLogout(token): Observable<Auth[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`
      })
    };
    return this.http.post<Auth[]>(`${environment.api.ha}/logout`, JSON.stringify(token), httpOptions).pipe(
      retry(0),
      catchError(this.errorHandl)
    );
  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    alert(errorMessage);
    return throwError(errorMessage);
  }



}

import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { DataServiceService } from '../../../services/data-service.service';
import { Storage } from '@ionic/storage';
import { Product } from '../../../models/product';
import { ProductData } from '../../../models/product-data';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { ModalController } from '@ionic/angular';
import { QrModalPage } from '../qr-modal/qr-modal.page';
import { AutoLogoutService } from '../../../services/auto-logout.service';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { QuantityModalPage } from '../quantity-modal/quantity-modal.page';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  status: any;
  segmentModel = 'order';
  token: any;
  haId: any;
  products: Product[] = [];
  productDatas: ProductData[] = [];
  today: any;
  total = 0;
  orderId: any;
  jobOrderNo: any;
  areaOfDelivery: any = '';
  deliveryDate: any;
  CustomerName: any;
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  value = '';
  scannedData: any;
  encodedData: any;
  OrderStatusId: any;
  isCompleted: string;
  orderStatus: any;
  color = 'danger';
  orders: any;
  dateReceived: any;
  posTransactionNumber: any;
  receivedBy: string;
  forDeliveryOrders: any;
  deliveryDate1: any;
  deliveryDate2: any;
  deliveryLevel: any;
  cost_summary: any;
  orderHaId: any;
  membershipNumber: any;
  membershipStatus: any;
  hasMembership: any;
  boxCategoryForm: FormGroup;
  boxCategoryData: Array<{ id: number, details: string }> = [];
  constructor(private orderService: OrderService,
    private dataServiceService: DataServiceService,
    private barcodeScanner: BarcodeScanner,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public modalController: ModalController,
    private autoLogout: AutoLogoutService) {
    this.status = 'order-details';
    this.orderId = this.activatedRoute.snapshot.paramMap.get('id');
    this.boxCategoryForm = this.formBuilder.group({
      boxCategory: ['', [Validators.required]],
      notes: [''],
    });
  }

  async ngOnInit() {
    this.token = await this.getStorage('token');
    this.haId = await this.getStorage('haId');
    this.orders = await this.getStorage('order');

    for (const order of this.orders) {
      this.jobOrderNo = order.job_order_number;
      this.cost_summary = order.cost_summary;
      this.hasMembership = order.has_membership;
      if (order.shipping_company_name) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_company_name}`;
      }
      if (order.shipping_building_no) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_building_no}`;
      }
      if (order.shipping_street_name) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_street_name}`;
      }
      if (order.shipping_barangay) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_barangay}`;
      }
      if (order.shipping_city) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_city}`;
      }
      if (order.holding_area) {
        this.receivedBy = `${order.holding_area.employee_id}`;
      }
      if (order.member.membership_number) {
        this.membershipNumber = order.member.membership_number;
        this.membershipStatus = order.member.membership_status;
      }
      this.posTransactionNumber = order.pos_transaction_number;
      this.dateReceived = order.ha_recieved_date;
      this.deliveryDate = order.delivery_date_time;
      this.deliveryLevel = order.delivery_level;
      this.CustomerName = `${order.shipping_first_name} ${order.shipping_last_name}`;
      this.OrderStatusId = order.order_status_id;
      this.orderHaId = order.ha_id;
    }
    await this.getData();
    if (this.OrderStatusId == 5 && this.orderHaId == null) {
      this.isCompleted = ``;
      this.orderStatus = ``;
      this.color = `danger`;
    } else if (this.OrderStatusId == 5 && this.orderHaId != null) {
      this.isCompleted = `completed`;
      this.orderStatus = `For Staging`;
      this.color = `success`;
    } else if (this.OrderStatusId == 6) {
      this.isCompleted = `completed`;
      this.orderStatus = `Out For Delivery`;
      this.color = `success`;
    } else if (this.OrderStatusId == 7) {
      this.isCompleted = `completed`;
      this.orderStatus = `Delivered`;
      this.color = `success`;
    }
  }
  async getData(): Promise<any> {
    // tslint:disable-next-line: prefer-const
    let result;
    try {
      const params = {
        order_id: this.orderId
      };
      await this.orderService.getOrderProducts(this.token, params).subscribe(async res => {
        if (res) {
          // tslint:disable-next-line: no-string-literal
          if (res['success']) {
            // tslint:disable-next-line: no-string-literal.
            this.getBoxCategoryList();
            this.products.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
            console.log(this.products);

            for (const product of this.products) {
              for (const data of product.data) {
                if ((data.deleted_at == null && data.is_replaced == 0) || (data.deleted_at != null && data.is_replaced == 1)) {
                  let isReplaced = 0;
                  let isOutOfStock = 0;
                  let status = 0;
                  let color = 'primary';
                  if (data.is_replaced == 1) {
                    isReplaced = 1;
                    color = 'success';
                    status = 2;
                  } else if (data.is_out_of_stock == 1) {
                    isOutOfStock = 1;
                    color = 'danger';
                    status = 3;
                  } else {
                    if (data.inventory.sku == "136") {
                      color = 'success';
                      status = 2;
                    }
                    this.total = this.total + Number(data.quantity);
                    for (let i = 1; i <= Number(data.quantity); i++) {
                      if (this.value == null) {
                        if (data.inventory.sku == "136") {
                          this.value = data.inventory.sku;
                        } else {
                          if (data.inventory.products.product_type_id == "2") {
                            if (data.sub_products[i - 1].weight != '0') {
                              this.value = data.sub_products[i - 1].scanned_upc
                            }
                          } else {
                            this.value = data.scanned_upc;
                          }
                        }
                      } else {
                        if (data.inventory.sku == "136") {
                          this.value = this.value + `\r\n` + data.inventory.sku;
                        } else {
                          if (data.inventory.products.product_type_id == "2") {
                            if (data.sub_products[i - 1].weight != '0') {
                              this.value = this.value + `\r\n` + data.sub_products[i - 1].scanned_upc
                            }
                          } else {
                            this.value = this.value + `\r\n` + data.scanned_upc;
                          }
                        }
                      }
                    }
                  }

                  //start of fresh items
                  console.log('first');
                  if (data.inventory.products.product_type_id == "2") {
                    if (data.is_replaced == 1) {
                      this.productDatas.push({
                        quantity: data.quantity,
                        is_same_prod_diff_size: data.is_same_prod_diff_size,
                        is_same_size_diff_brand: data.is_same_size_diff_brand,
                        is_no_substitution: data.is_no_substitution,
                        is_out_of_stock: isOutOfStock,
                        is_dc_delivery: data.is_dc_delivery,
                        delivery_date_time: data.dc_schedule,
                        id: data.id,
                        product_id: data.product_id,
                        sku: data.inventory.sku,
                        department_id: data.inventory.department_id,
                        sku_location: data.inventory.products.sku_location,
                        name: data.inventory.products.name,
                        featured_image: this.url(data.inventory.products.featured_image),
                        product_type_id: data.inventory.products.product_type_id,
                        upc: data.scanned_upc,
                        price_history: data.price_history,
                        weight: '1',
                        color: color,
                        isEdited: 0,
                        isReplaced: isReplaced,
                        status: status
                      });
                    } else {
                      for (let i = 1; i <= Number(data.quantity); i++) {
                        if (data.sub_products.length > 0) {
                          let weight;
                          if (data.sub_products.length >= i) {
                            if (data.sub_products[i - 1].weight != '0') {
                              weight = data.sub_products[i - 1].weight;
                              color = 'success';
                              status = 1;
                              isOutOfStock = 0;
                            } else {
                              weight = data.sub_products[i - 1].weight;
                              isOutOfStock = 1;
                              color = 'success';
                              status = 3;
                              this.total = this.total - 1;
                            }
                          } else {
                            weight = '1';
                            isOutOfStock = 0;
                            color = 'primary';
                            status = 0;
                          }
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: weight,
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        } else {
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: '1',
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        }
                      }
                    }
                  } else {
                    if (data.scanned_upc != null) {
                      color = 'success';
                      status = 1;
                    }
                    this.productDatas.push({
                      quantity: data.quantity,
                      is_same_prod_diff_size: data.is_same_prod_diff_size,
                      is_same_size_diff_brand: data.is_same_size_diff_brand,
                      is_no_substitution: data.is_no_substitution,
                      is_out_of_stock: isOutOfStock,
                      is_dc_delivery: data.is_dc_delivery,
                      delivery_date_time: data.dc_schedule,
                      id: data.id,
                      product_id: data.product_id,
                      sku: data.inventory.sku,
                      department_id: data.inventory.department_id,
                      sku_location: data.inventory.products.sku_location,
                      name: data.inventory.products.name,
                      featured_image: this.url(data.inventory.products.featured_image),
                      product_type_id: data.inventory.products.product_type_id,
                      upc: '',
                      price_history: data.price_history,
                      weight: '1',
                      color: color,
                      isEdited: 0,
                      isReplaced: isReplaced,
                      status: status
                    });
                  }
                  //end of fresh items 
                }
              }
            }
            this.value = this.value.trim();
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async doScan() {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      orientation: 'portrait',
    };
    this.barcodeScanner.scan(options).then(async barcodeData => {
      this.scannedData = barcodeData;
      // tslint:disable-next-line: no-string-literal
      this.encodedData = this.scannedData['text'];
      if (this.encodedData === this.posTransactionNumber) {
        this.isCompleted = `completed`;
        this.orderStatus = `For Staging`;
        this.color = `success`;
        if (this.deliveryLevel != 3) {
          const params = {
            order_id: this.orderId,
            ha_id: this.haId,
            order_status_id: 5
          };
          // tslint:disable-next-line: no-string-literal
          //this.receivedBy = `${res['data']['holding_area']['employee_id']}`;
          // tslint:disable-next-line: no-string-literal
          //this.dateReceived = res['data']['ha_recieved_date'];
          // tslint:disable-next-line: no-string-literal
          this.token = await this.getStorage('token');

          await this.orderService.updateOrder(this.token, params).subscribe(async (res) => {
            // tslint:disable-next-line: no-string-literal
            if (res['success']) {
              this.isCompleted = `completed`;
              this.orderStatus = `For Staging`;
              this.color = `success`;
              // tslint:disable-next-line: no-string-literal
              this.receivedBy = `${res['data']['ha_id']}`;
              // tslint:disable-next-line: no-string-literal
              this.dateReceived = res['data']['ha_recieved_date'];
              // tslint:disable-next-line: no-string-literal
              alert(res['message']);
              await this.router.navigate(['menu/rider']);
            }
          });
        } else {
          this.isCompleted = `completed`;
          this.orderStatus = `For Staging`;
          this.color = `success`;
        }
      } else {
        alert(`Invalid Receipt ${this.encodedData}`);
      }
      this.today = new Date().toISOString();
    }).catch(err => {
      console.log('Error', err);
    });
  }

  async doInput() {
    const modal = await this.modalController.create({
      component: QuantityModalPage,
      componentProps: {}
    });
    modal.onDidDismiss().then(async (dataReturned) => {
      if (dataReturned != null) {
        this.encodedData = dataReturned.data;
        if (this.encodedData === this.posTransactionNumber) {
          this.isCompleted = `completed`;
          this.orderStatus = `For Staging`;
          this.color = `success`;
          if (this.deliveryLevel != 3) {
            const params = {
              order_id: this.orderId,
              ha_id: this.haId,
              order_status_id: 5
            };
            // tslint:disable-next-line: no-string-literal
            //this.receivedBy = `${res['data']['holding_area']['employee_id']}`;
            // tslint:disable-next-line: no-string-literal
            //this.dateReceived = res['data']['ha_recieved_date'];
            // tslint:disable-next-line: no-string-literal
            this.token = await this.getStorage('token');

            await this.orderService.updateOrder(this.token, params).subscribe(async (res) => {
              // tslint:disable-next-line: no-string-literal
              if (res['success']) {
                this.isCompleted = `completed`;
                this.orderStatus = `For Staging`;
                this.color = `success`;
                // tslint:disable-next-line: no-string-literal
                this.receivedBy = `${res['data']['ha_id']}`;
                // tslint:disable-next-line: no-string-literal
                this.dateReceived = res['data']['ha_recieved_date'];
                // tslint:disable-next-line: no-string-literal
                alert(res['message']);
                await this.router.navigate(['menu/rider']);
              }
            });
          }
        } else {
          alert(`Invalid Receipt ${this.encodedData}`);
        }
        this.today = new Date().toISOString();
      }
    });
    return await modal.present();
    // tslint:disable-next-line: no-string-literal

  }

  async doUpdate() {
    if (this.boxCategoryList.value) {
      const params = {
        box_category_id: this.boxCategoryList.value,
        order_id: this.orderId
      };
      await this.orderService.updateBoxCategory(this.token, params).subscribe(async (res) => {
        // tslint:disable-next-line: no-string-literal
        if (res['success']) {
          const params = {
            order_id: this.orderId,
            ha_id: this.haId,
            order_status_id: 5
          };
          // tslint:disable-next-line: no-string-literal
          //this.receivedBy = `${res['data']['holding_area']['employee_id']}`;
          // tslint:disable-next-line: no-string-literal
          //this.dateReceived = res['data']['ha_recieved_date'];
          // tslint:disable-next-line: no-string-literal
          this.token = await this.getStorage('token');

          await this.orderService.updateOrder(this.token, params).subscribe(async (res) => {
            // tslint:disable-next-line: no-string-literal
            if (res['success']) {
              // tslint:disable-next-line: no-string-literal
              this.receivedBy = `${res['data']['ha_id']}`;
              // tslint:disable-next-line: no-string-literal
              this.dateReceived = res['data']['ha_recieved_date'];
              // tslint:disable-next-line: no-string-literal
              alert(res['message']);
              await this.router.navigate(['menu/lbc']);
            }
          });
        } else {
          // tslint:disable-next-line: no-string-literal
          alert(res['message']);
        }
      });
    }
  }

  async getBoxCategoryList() {
    await this.orderService.getBoxCategory(this.token).subscribe(async res => {
      if (res) {
        // tslint:disable-next-line: no-string-literal
        if (res['success']) {
          // tslint:disable-next-line: no-string-literal
          for (let data of res['data']) {
            let vkey;
            let details
            Object.keys(data)
              .forEach(function eachKey(key) {
                vkey = key;
                details = data[key];
              });
            this.boxCategoryData.push({
              id: vkey,
              details: details
            })
          }
        }
      }
    });
  }

  async doRefresh() {
    let result;
    try {
      await this.orderService.getAvailableOnForDeliveryOrders(this.token).subscribe(async res => {
        if (res) {
          if (res['success']) {
            for (let data of res['data']) {
              if (data.id == this.orderId) {
                this.OrderStatusId = data.order_status_id;
                if (data.holding_area) {
                  this.receivedBy = `${data.holding_area.employee_id}`;
                }
                if (this.OrderStatusId == 5 && data.ha_id == null) {
                  this.isCompleted = ``;
                  this.orderStatus = ``;
                  this.color = `danger`;
                } else if (this.OrderStatusId == 5 && data.ha_id != null) {
                  this.isCompleted = `completed`;
                  this.orderStatus = `For Staging`;
                  this.color = `success`;
                } else if (this.OrderStatusId == 6) {
                  this.isCompleted = `completed`;
                  this.orderStatus = `Out For Delivery`;
                  this.color = `success`;
                } else if (this.OrderStatusId == 7) {
                  this.isCompleted = `completed`;
                  this.orderStatus = `Delivered`;
                  this.color = `success`;
                }
                break;
              }
            }
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async openInputModal() {
    const modal = await this.modalController.create({
      component: QuantityModalPage,
      componentProps: {}
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned != null) {
        this.encodedData = dataReturned.data;
        this.today = new Date().toISOString();
      }
    });
    return await modal.present();
  }

  async openQRQModal() {
    const modal = await this.modalController.create({
      component: QrModalPage,
      componentProps: {
        qr: this.value
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }
  async openImage(img) {
    const modal = await this.modalController.create({
      component: ImageModalPage,
      componentProps: {
        img
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }
  async getStorage(key): Promise<any> {
    try {
      const result = await this.storage.get(key);
      return result;
    }
    catch (e) { console.log(e); }
  }
  url(str) {
    const cleanUrl = JSON.parse(str);
    if (cleanUrl) {
      return 'https://webtest.snrshopping.com/' + cleanUrl[0].toString();
    }
  }
  startTime() {
    const intervalVar = setInterval(function () {
      this.today = new Date().toISOString();
    }.bind(this), 500);
  }

  get boxCategoryList() {
    return this.boxCategoryForm.get('boxCategory');
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LbcPage } from './lbc.page';

describe('LbcPage', () => {
  let component: LbcPage;
  let fixture: ComponentFixture<LbcPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LbcPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LbcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

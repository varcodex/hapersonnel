import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LbcPageRoutingModule } from './lbc-routing.module';

import { LbcPage } from './lbc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LbcPageRoutingModule
  ],
  declarations: [LbcPage]
})
export class LbcPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LbcPage } from './lbc.page';

const routes: Routes = [
  {
    path: '',
    component: LbcPage,
  },
  {
    path: 'products/:id',
    loadChildren: () => import('./products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: 'qr-modal',
    loadChildren: () => import('./qr-modal/qr-modal.module').then( m => m.QrModalPageModule)
  },
  {
    path: 'image-modal',
    loadChildren: () => import('./image-modal/image-modal.module').then( m => m.ImageModalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LbcPageRoutingModule {}

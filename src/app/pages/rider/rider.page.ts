import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { OrderService } from '../../services/order.service';
import { Order } from '../../models/order';
import { Product } from '../../models/product';
import { Router } from '@angular/router';
import { DataServiceService } from '../../services/data-service.service';
import { formatDate } from '@angular/common';
import { AutoLogoutService } from '../../services/auto-logout.service';

@Component({
  selector: 'app-rider',
  templateUrl: './rider.page.html',
  styleUrls: ['./rider.page.scss'],
})
export class RiderPage implements OnInit {
  token: any;
  haId: any;
  orders: Order[] = [];
  forDeliveryOrders: Order[] = [];
  products: Product[] = [];
  today: any;
  deliveryDate: string;
  constructor(public router: Router,
    private orderService: OrderService,
    private dataServiceService: DataServiceService,
    private storage: Storage,
    private autoLogout: AutoLogoutService) { }

    async ngOnInit() {
      this.token = await this.getStorage('token');
      this.haId = await this.getStorage('haId');
      await this.getData();
    }

    async doRefresh(){
      this.orders = [];
      this.getData();
    }
  
    async goToRider(id: any, i: string | number){
      const data: any[] = [];
      for (const order of this.orders){
        let holdingArea: { firstname: string; middlename: string; lastname: string; employee_id: string; };
        if (order.data[i].holding_area){
        holdingArea = {
          firstname: order.data[i].holding_area.firstname,
          middlename: order.data[i].holding_area.middlename,
          lastname: order.data[i].holding_area.lastname,
          employee_id: order.data[i].holding_area.employee_id,
        };
      }else{
        holdingArea = null;
      }
        data.push({
        id: order.data[i].id,
        member_id : order.data[i].member_id,
        checkedout_date :  order.data[i].checkedout_date,
        stores_id : order.data[i].stores_id,
        job_order_number :  order.data[i].job_order_number,
        customer_service_id : order.data[i].customer_service_id,
        delivery_date_time :  this.deliveryDate,
        delivery_date_time2 : order.data[i].delivery_date_time2,
        shipping_first_name :  order.data[i].shipping_first_name,
        shipping_last_name :  order.data[i].shipping_last_name,
        shipping_company_name :  order.data[i].shipping_company_name,
        shipping_street_name :  order.data[i].shipping_street_name,
        shipping_building_no :  order.data[i].shipping_building_no,
        shipping_phone :  order.data[i].shipping_phone,
        shipping_barangay :  order.data[i].shipping_barangay,
        shipping_city :  order.data[i].shipping_city,
        recieved_by_member : order.data[i].recieved_by_member,
        delivery_level : order.data[i].delivery_level,
        order_status_id : order.data[i].order_status_id,
        shoppers_id : order.data[i].shoppers_id,
        has_membership: order.data[i].has_membership,
        cost_summary :  JSON.parse(order.data[i].cost_summary),
        notification_shopper_id : order.data[i].notification_shopper_id,
        ha_id: order.data[i].ha_id,
        ha_recieved_date: order.data[i].ha_recieved_date,
        delivered_at: order.data[i].delivered_at,
        pos_transaction_number:  order.data[i].pos_transaction_number,
        member: {
          membership_number: order.data[i].member.membership_number,
          membership_status: order.data[i].member.membership_status,
        },
        holding_area: holdingArea
      });
      }
      await this.dataServiceService.setOrder(data);
      await this.router.navigate(['menu/rider/products', id]);
    }
  
    async getData(): Promise<any> {
      // tslint:disable-next-line: prefer-const
      let result: any;
      try {
        await this.orderService.getAvailableOnForDeliveryOrdersLastMile(this.token).subscribe(async res => {
          if (res){
            // tslint:disable-next-line: no-string-literal
            if (res['success']){
              // tslint:disable-next-line: no-string-literal
              this.orders.push({success : res['success'], data : res['data'], message : res['message'], code: res['code']});
              for(let order of this.orders){
                for(let data of order.data){
                    if(data.delivery_level == '3'){
                      let d1:Date;
                      let d2:Date;
                      d1 = new Date(data.checkedout_date);
                      d2 = new Date(data.checkedout_date);
                      d1.setDate(d1.getDate() + 3);
                      d2.setDate(d2.getDate()  + 5);
                      this.deliveryDate = `${formatDate(d1, 'MMMM dd', 'en-PH', '+08:00')} - ${formatDate(d2, 'dd', 'en-PH', '+08:00')}`;
                    }else{
                      this.deliveryDate = formatDate(data.delivery_date_time, 'EEEE, d MMMM yyyy h:mm ', 'en-PH', '+08:00');
                    }
                }
              }
            }else{
              alert('Connection time out. Please try again or contact the administrator!');
            }
          }else{
            alert('Connection time out. Please try again or contact the administrator!');
          }
        });
        return result;
      }
      catch (e) { console.log(e); }
    }
    async getStorage(key: string): Promise<any> {
      try {
          const result =  await this.storage.get(key);
          return result;
      }
      catch (e) { console.log(e); }
    }

}

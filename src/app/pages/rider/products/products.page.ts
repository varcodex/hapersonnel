import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { DataServiceService } from '../../../services/data-service.service';
import { Storage } from '@ionic/storage';
import { Product } from '../../../models/product';
import { Rider } from '../../../models/rider';
import { ProductData } from '../../../models/product-data';
import { ActivatedRoute } from '@angular/router';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import { ModalController } from '@ionic/angular';
import { QrModalPage } from '../qr-modal/qr-modal.page';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { ListPage } from '../list/list.page';
import { AutoLogoutService } from '../../../services/auto-logout.service';
export interface Logs {
  id: string,
  status: string,
  rider_name: string,
  timestamp: string
}
@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})

export class ProductsPage implements OnInit {
  status: any;
  segmentModel = 'order';
  token: any;
  haId: any;
  products: Product[] = [];
  productDatas: ProductData[] = [];
  today: any;
  total = 0;
  orderId: any;
  jobOrderNo: any;
  areaOfDelivery: any = '';
  deliveryDate: any;
  CustomerName: any;
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  value = '';
  scannedData: any;
  encodedData: any;
  OrderStatusId: any;
  isCompleted: string;
  orderStatus: any;
  color = 'danger';
  orders: any;
  dateReceived: any;
  posTransactionNumber: any;
  receivedBy: string;
  forDeliveryOrders: any;
  deliveryDate1: any;
  deliveryDate2: any;
  deliveryLevel: any;
  cost_summary: any;
  riderRes: Rider[] = [];
  orderHaId: any;
  membershipNumber: any;
  membershipStatus: any;
  hasMembership: any;
  timer: boolean = false;
  logs: Logs[] = [];
  constructor(private orderService: OrderService,
    private dataServiceService: DataServiceService,
    private barcodeScanner: BarcodeScanner,
    private storage: Storage,
    private autoLogout: AutoLogoutService,
    private activatedRoute: ActivatedRoute,
    public modalController: ModalController) {
    this.status = 'order-details';
    this.orderId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  async ngOnInit() {
    this.token = await this.getStorage('token');
    this.haId = await this.getStorage('haId');
    this.orders = await this.getStorage('order');
    console.log(this.orders)
    for (const order of this.orders) {
      this.jobOrderNo = order.job_order_number;
      this.cost_summary = order.cost_summary;
      this.hasMembership = order.has_membership;
      if (order.shipping_company_name) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_company_name}`;
      }
      if (order.shipping_building_no) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_building_no}`;
      }
      if (order.shipping_street_name) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_street_name}`;
      }
      if (order.shipping_barangay) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_barangay}`;
      }
      if (order.shipping_city) {
        this.areaOfDelivery = `${this.areaOfDelivery} ${order.shipping_city}`;
      }
      if (order.holding_area) {
        this.receivedBy = `${order.holding_area.employee_id}`;
      }
      if (order.member.membership_number) {
        this.membershipNumber = order.member.membership_number;
        this.membershipStatus = order.member.membership_status;
      }
      this.posTransactionNumber = order.pos_transaction_number;
      this.dateReceived = order.ha_recieved_date;
      this.deliveryDate = order.delivery_date_time;
      this.deliveryLevel = order.delivery_level;
      this.CustomerName = `${order.shipping_first_name} ${order.shipping_last_name}`;
      this.OrderStatusId = order.order_status_id;
      this.orderHaId = order.ha_id;
    }
    await this.getData();
    if (this.OrderStatusId == 5 && this.orderHaId == null) {
      this.isCompleted = ``;
      this.orderStatus = ``;
      this.color = `danger`;
    } else if (this.OrderStatusId == 5 && this.orderHaId != null) {
      this.isCompleted = `completed`;
      this.orderStatus = `For Delivery`;
      this.color = `success`;
    } else if (this.OrderStatusId == 6) {
      this.isCompleted = `completed`;
      this.orderStatus = `Out For Delivery`;
      this.color = `success`;
    } else if (this.OrderStatusId == 7) {
      this.isCompleted = `completed`;
      this.orderStatus = `Delivered`;
      this.color = `success`;
    }
  }

  async getJobsPerCart(): Promise<any> {
    this.riderRes = [];
    this.logs = [];
    try {
      const params = {
        cart_id: this.orderId
      };
      await this.orderService.getJobsPerCart(this.token, params).subscribe(async res => {
        if (res) {
          if (res['success']) {
            for (const result of res['results']) {
              try {
                const params = {
                  fleet_job_id: result['fleet_job_id']
                };
                if (result['fleet_status'] != 'DONE') {
                  this.timer = true;
                }
                this.riderRes.push({
                  cart_id: result['cart_id'],
                  fleet_job_id: result['fleet_job_id'],
                  fleet_rider_id: result['fleet_rider_id'],
                  fleet_rider_name: result['fleet_rider_name'],
                  fleet_status: result['fleet_status']
                });
                await this.orderService.getJobStatus(this.token, params).subscribe(async res => {
                  console.log(res);
                  if (res) {
                    console.log(res);
                    if (res['success']) {
                      if (res['data']['logs'].length > 0) {
                        for (const log of res['data']['logs']) {
                          this.logs.push({
                            id: result['fleet_job_id'],
                            status: log['status'],
                            rider_name: log['rider_name'],
                            timestamp: log['timestamp'],
                          });
                        }
                      }
                    } else {
                      alert('Connection time out. Please try again or contact the administrator!');
                    }
                  } else {
                    alert('Connection time out. Please try again or contact the administrator!');
                  }
                });
              }
              catch (e) { console.log(e); }
            }
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
    }
    catch (e) { console.log(e); }
  }

  async getData(): Promise<any> {
    // tslint:disable-next-line: prefer-const
    let result;
    try {
      const params = {
        order_id: this.orderId
      };
      await this.orderService.getOrderProducts(this.token, params).subscribe(async res => {
        if (res) {
          if (res['success']) {
            this.products.push({ success: res['success'], data: res['data'], message: res['message'], code: res['code'] });
            for (const product of this.products) {
              for (const data of product.data) {
                if ((data.deleted_at == null && data.is_replaced == 0) || (data.deleted_at != null && data.is_replaced == 1)) {
                  let isReplaced = 0;
                  let isOutOfStock = 0;
                  let status = 0;
                  let color = 'primary';
                  if (data.is_replaced == 1) {
                    isReplaced = 1;
                    color = 'success';
                    status = 2;
                  } else if (data.is_out_of_stock == 1) {
                    isOutOfStock = 1;
                    color = 'danger';
                    status = 3;
                  } else {
                    if (data.inventory.sku == "136") {
                      color = 'success';
                      status = 2;
                    }
                    this.total = this.total + Number(data.quantity);
                    for (let i = 1; i <= Number(data.quantity); i++) {
                      if (this.value == null) {
                        if (data.inventory.sku == "136") {
                          this.value = data.inventory.sku;
                        } else {
                          if (data.inventory.products.product_type_id == "2") {
                            if (data.sub_products[i - 1].weight != '0') {
                              this.value = data.sub_products[i - 1].scanned_upc
                            }
                          } else {
                            this.value = data.scanned_upc;
                          }
                        }
                      } else {
                        if (data.inventory.sku == "136") {
                          this.value = this.value + `\r\n` + data.inventory.sku;
                        } else {
                          if (data.inventory.products.product_type_id == "2") {
                            if (data.sub_products[i - 1].weight != '0') {
                              this.value = this.value + `\r\n` + data.sub_products[i - 1].scanned_upc
                            }
                          } else {
                            this.value = this.value + `\r\n` + data.scanned_upc;
                          }
                        }
                      }
                    }
                  }

                  //start of fresh items
                  if (data.inventory.products.product_type_id == "2") {
                    if (data.is_replaced == 1) {
                      this.productDatas.push({
                        quantity: data.quantity,
                        is_same_prod_diff_size: data.is_same_prod_diff_size,
                        is_same_size_diff_brand: data.is_same_size_diff_brand,
                        is_no_substitution: data.is_no_substitution,
                        is_out_of_stock: isOutOfStock,
                        is_dc_delivery: data.is_dc_delivery,
                        delivery_date_time: data.dc_schedule,
                        id: data.id,
                        product_id: data.product_id,
                        sku: data.inventory.sku,
                        department_id: data.inventory.department_id,
                        sku_location: data.inventory.products.sku_location,
                        name: data.inventory.products.name,
                        featured_image: this.url(data.inventory.products.featured_image),
                        product_type_id: data.inventory.products.product_type_id,
                        upc: data.scanned_upc,
                        price_history: data.price_history,
                        weight: '1',
                        color: color,
                        isEdited: 0,
                        isReplaced: isReplaced,
                        status: status
                      });
                    } else {
                      for (let i = 1; i <= Number(data.quantity); i++) {
                        if (data.sub_products.length > 0) {
                          let weight;
                          if (data.sub_products.length >= i) {
                            if (data.sub_products[i - 1].weight != '0') {
                              weight = data.sub_products[i - 1].weight;
                              color = 'success';
                              status = 1;
                              isOutOfStock = 0;
                            } else {
                              weight = data.sub_products[i - 1].weight;
                              isOutOfStock = 1;
                              color = 'success';
                              status = 3;
                              this.total = this.total - 1;
                            }
                          } else {
                            weight = '1';
                            isOutOfStock = 0;
                            color = 'primary';
                            status = 0;
                          }
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: weight,
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        } else {
                          this.productDatas.push({
                            quantity: "1",
                            is_same_prod_diff_size: data.is_same_prod_diff_size,
                            is_same_size_diff_brand: data.is_same_size_diff_brand,
                            is_no_substitution: data.is_no_substitution,
                            is_out_of_stock: isOutOfStock,
                            is_dc_delivery: data.is_dc_delivery,
                            delivery_date_time: data.dc_schedule,
                            id: data.id,
                            product_id: data.product_id,
                            sku: data.inventory.sku,
                            department_id: data.inventory.department_id,
                            sku_location: data.inventory.products.sku_location,
                            name: data.inventory.products.name,
                            featured_image: this.url(data.inventory.products.featured_image),
                            product_type_id: data.inventory.products.product_type_id,
                            upc: '',
                            price_history: data.inventory.unit_price,
                            weight: '1',
                            color: color,
                            isEdited: 0,
                            isReplaced: isReplaced,
                            status: status
                          });
                        }
                      }
                    }
                  } else {
                    if (data.scanned_upc != null) {
                      color = 'success';
                      status = 1;
                    }
                    this.productDatas.push({
                      quantity: data.quantity,
                      is_same_prod_diff_size: data.is_same_prod_diff_size,
                      is_same_size_diff_brand: data.is_same_size_diff_brand,
                      is_no_substitution: data.is_no_substitution,
                      is_out_of_stock: isOutOfStock,
                      is_dc_delivery: data.is_dc_delivery,
                      delivery_date_time: data.dc_schedule,
                      id: data.id,
                      product_id: data.product_id,
                      sku: data.inventory.sku,
                      department_id: data.inventory.department_id,
                      sku_location: data.inventory.products.sku_location,
                      name: data.inventory.products.name,
                      featured_image: this.url(data.inventory.products.featured_image),
                      product_type_id: data.inventory.products.product_type_id,
                      upc: '',
                      price_history: data.price_history,
                      weight: '1',
                      color: color,
                      isEdited: 0,
                      isReplaced: isReplaced,
                      status: status
                    });
                  }
                  //end of fresh items 
                }
              }
            }
            this.value = this.value.trim();
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async doRefresh() {
    let result;
    try {
      await this.orderService.getAvailableOnForDeliveryOrders(this.token).subscribe(async res => {
        if (res) {
          if (res['success']) {
            for (let data of res['data']) {
              if (data.id == this.orderId) {
                this.OrderStatusId = data.order_status_id;
                if (data.holding_area) {
                  this.receivedBy = `${data.holding_area.employee_id}`;
                }
                if (this.OrderStatusId == 5 && data.ha_id == null) {
                  this.isCompleted = ``;
                  this.orderStatus = ``;
                  this.color = `danger`;
                } else if (this.OrderStatusId == 5 && data.ha_id != null) {
                  this.isCompleted = `completed`;
                  this.orderStatus = `For Staging`;
                  this.color = `success`;
                } else if (this.OrderStatusId == 6) {
                  this.isCompleted = `completed`;
                  this.orderStatus = `Out For Delivery`;
                  this.color = `success`;
                } else if (this.OrderStatusId == 7) {
                  this.isCompleted = `completed`;
                  this.orderStatus = `Delivered`;
                  this.color = `success`;
                }
                break;
              }
            }
          } else {
            alert('Connection time out. Please try again or contact the administrator!');
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
      return result;
    }
    catch (e) { console.log(e); }
  }

  async updateJob(riderID, riderName, fleetJobID) {
    const params = {
      rider_id: riderID,
      rider_name: riderName,
      fleet_job_id: fleetJobID,
      cart_id: this.orderId,
      status: "STARTED"
    }
    try {
      await this.orderService.updateJobs(this.token, params).subscribe(async res => {
        if (res) {
          if (res['success']) {
            this.getJobsPerCart();
            const params = {
              order_id: this.orderId,
              ha_id: this.orderHaId,
              order_status_id: 6
            };
            await this.orderService.updateOrder(this.token, params).subscribe(async (res) => {
              if (res['success']) {
                this.isCompleted = `completed`;
                this.orderStatus = `Out For Delivery`;
                this.color = `success`;
                this.receivedBy = `${res['data']['holding_area']['employee_id']}`;
              } else {
                alert(res['message']);
              }
            });
          } else {
            alert(res['message']);
          }
        } else {
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
    }
    catch (e) { console.log(e); }
  }

  async openQRQModal() {
    const modal = await this.modalController.create({
      component: QrModalPage,
      componentProps: {
        qr: this.value
      }
    });
    modal.onDidDismiss().then((dataReturned) => {

    });
    return await modal.present();
  }

  async openImage(img) {
    const modal = await this.modalController.create({
      component: ImageModalPage,
      componentProps: {
        img
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
    });
    return await modal.present();
  }

  async openRiderList(fleetJobID) {
    const cartID = this.orderId;
    const modal = await this.modalController.create({
      component: ListPage,
      componentProps: {
        fleetJobID,
        cartID
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned != null) {
        const result = dataReturned.data;
        if (result != null) {
          this.getJobsPerCart();
        }
      }
    });
    return await modal.present();
  }

  async getStorage(key): Promise<any> {
    try {
      const result = await this.storage.get(key);
      return result;
    }
    catch (e) { console.log(e); }
  }
  url(str) {
    const cleanUrl = JSON.parse(str);
    if (cleanUrl) {
      return 'https://webtest.snrshopping.com/' + cleanUrl[0].toString();
    }
  }


}

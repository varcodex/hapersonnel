import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { OrderService } from '../../../services/order.service';
import { AutoLogoutService } from '../../../services/auto-logout.service';
@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  token: any;
  riderList: any = [];
  fleetJobID: any;
  cartID: any;
  constructor(private modalController: ModalController,
    private storage: Storage,
    private navParams: NavParams,
    private orderService: OrderService,
    private autoLogout: AutoLogoutService) { }

  async ngOnInit() {
    this.token = await this.getStorage('token');
    this.fleetJobID = this.navParams.data.fleetJobID;
    this.cartID = this.navParams.data.cartID;
    await this.getData();
  }

  async closeModal() {
    await this.modalController.dismiss(null);
  }

  async updateJob(riderID, firstName, lastName) {
    const params = {
      rider_id: riderID,
      rider_name: `${firstName} ${lastName}`,
      fleet_job_id: this.fleetJobID,
      cart_id: this.cartID,
      status: "ASSIGNED"
    }
    console.log(params);
    try {
      await this.orderService.updateJobs(this.token, params).subscribe(async res => {
        if (res){
          // tslint:disable-next-line: no-string-literal
          if (res['success']){
            // tslint:disable-next-line: no-string-literal
            const data = true;
            await this.modalController.dismiss(data);
            console.log(res);
          }else{
            alert('Connection time out. Please try again or contact the administrator!');
          }
        }else{
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
    }
    catch (e) { console.log(e); }
  }

  async getData(): Promise<any> {
    // tslint:disable-next-line: prefer-const
    try {
      await this.orderService.getRiders(this.token).subscribe(async res => {
        if (res){
          // tslint:disable-next-line: no-string-literal
          if (res['success']){
            // tslint:disable-next-line: no-string-literal
            this.riderList.push(res);
          }else{
            alert('Connection time out. Please try again or contact the administrator!');
          }
        }else{
          alert('Connection time out. Please try again or contact the administrator!');
        }
      });
    }
    catch (e) { console.log(e); }
  }
  async getStorage(key: string): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch (e) { console.log(e); }
  }
}

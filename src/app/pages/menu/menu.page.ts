import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular'; 
import { Router, RouterEvent } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../services/auth.service';
import { DataServiceService } from '../../services/data-service.service';
import { Auth } from '../../models/auth';
import { AutoLogoutService } from '../../services/auto-logout.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  haName: any;
  token: any;
  auth: Auth[] = [];
  pages = [];

  selectedPath = '';
  role: string;

  constructor(private router: Router,
    private authservice: AuthService,
    private dataServiceService: DataServiceService,
    private storage: Storage,
    private menu: MenuController,
    private autoLogout: AutoLogoutService) { 
      this.dataServiceService.getToken();
      this.dataServiceService.gethaName();
    }

  async ngOnInit() {
    await this.dataServiceService.castRole.subscribe(data=> this.role = data);
    if(this.role == 'holdingArea'){
      this.pages.push({
        icon: 'analytics',
        title: 'Dashboard',
        url: '/menu/dashboard'
      },
      {
        icon: 'person',
        title: 'My Account',
        url: '/menu/my-account'
      },
      {
        icon: 'star',
        title: 'For Staging',
        url: '/menu/for-delivery-order'
      },
      {
        icon: 'people',
        title: 'Rider',
        url: '/menu/rider'
      });
    }else if(this.role = 'holdingAreaLevelThree'){
      this.pages.push({
        icon: 'analytics',
        title: 'Dashboard',
        url: '/menu/dashboard'
      },
      {
        icon: 'person',
        title: 'My Account',
        url: '/menu/my-account'
      },
      {
        icon: 'star',
        title: 'For Staging',
        url: '/menu/for-delivery-order'
      },
      {
        icon: 'cube',
        title: 'LBC',
        url: '/menu/lbc'
      })
    }
    this.router.events.subscribe(async (event: RouterEvent) => {
      if(event && event.url){
        this.selectedPath = event.url;
      }
    });
    await this.dataServiceService.casthaName.subscribe(data=> this.haName = data);
  }

  async onlogout() {
    this.token = await this.getStorage('token');
    this.authservice.onLogout(this.token).subscribe(async res => {
      if(res['success']){
        this.menu.close();
        alert(res['message']);
        await this.clearStorage();
        this.router.navigate(['login']); 
      }else{
        alert('Invalid credentials/Connection time out. Please contact the administrator!');
      }  
    });
  }

  async clearStorage(): Promise<any> {
    try {
        const result =  await this.storage.clear();
        return result;
    }
    catch(e) { console.log(e) }
  }

  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch(e) { console.log(e) }
  }

}

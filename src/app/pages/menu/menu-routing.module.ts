import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'my-account',
        loadChildren: () => import('../my-account/my-account.module').then( m => m.MyAccountPageModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('../dashboard/dashboard.module').then( m => m.DashboardPageModule)
      },
      {
        path: 'for-delivery-order',
        loadChildren: () => import('../for-delivery-order/for-delivery-order.module').then( m => m.ForDeliveryOrderPageModule)
      },
      {
        path: 'courier',
        loadChildren: () => import('../courier/courier.module').then( m => m.CourierPageModule)
      },
      {
        path: 'rider',
        loadChildren: () => import('../rider/rider.module').then( m => m.RiderPageModule)
      },
      {
        path: 'lbc',
        loadChildren: () => import('../lbc/lbc.module').then( m => m.LbcPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { DataServiceService } from '../../services/data-service.service';
import { Auth } from '../../models/auth';
import { Storage } from '@ionic/storage';
import { AutoLogoutService } from '../../services/auto-logout.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  imgSrc : String = '/assets/imgs/logo.png';
  auth: Auth[] = [];
  ha_id: any;
  haName: any;

  constructor(public router: Router,
    private authservice: AuthService,
    private dataServiceService: DataServiceService,
    private storage: Storage,
    private autoLogout: AutoLogoutService) { }

  ngOnInit() {}
  async onlogIn(employee_id, password) {
    this.storage.clear();
    this.auth = [];
    let credentials = {
      employee_id: employee_id.value,
      password: password.value
    }
    
    await this.authservice.onLogin(credentials).subscribe(async res => {
      if(res){
        this.auth.push({success : res['success'], data : res['data'], message : res['message']});
        for(let auth of this.auth){
          if(auth.success){
            if(Object.keys(auth.data).length > 0){
              await this.dataServiceService.sethaName(auth.data.name);
              await this.dataServiceService.setToken(auth.data.token);
              await this.dataServiceService.setRole(auth.data.role_slug);
              await this.dataServiceService.sethaId(auth.data.id);
              await this.router.navigate(['menu/dashboard']);
            }
          }else{
            alert('Invalid credentials/Connection time out. Please try again or contact the administrator!');
          }
        }
      }else{
        alert('Invalid credentials/Connection time out. Please try again or contact the administrator!');
      }
    });
  }
}

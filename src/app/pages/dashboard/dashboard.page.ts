import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DataServiceService } from '../../services/data-service.service';
import { AutoLogoutService } from '../../services/auto-logout.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  today:any;
  haName: any;
  constructor(private storage: Storage,
    private dataServiceService: DataServiceService,
    private autoLogout: AutoLogoutService) { 
      this.startTime();
      this.dataServiceService.gethaName();
    }

  async ngOnInit() {
    await this.dataServiceService.casthaName.subscribe(data=> this.haName = data);
  }

  startTime() {
    var intervalVar = setInterval(function () {
      this.today = new Date().toISOString();
    }.bind(this),500)
  }
  async getStorage(key): Promise<any> {
    try {
        const result =  await this.storage.get(key);
        return result;
    }
    catch(e) { console.log(e) }
  }

}
